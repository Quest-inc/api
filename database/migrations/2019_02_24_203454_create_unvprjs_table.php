<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnvprjsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unvprjs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Project_Ref');
            $table->string('Country');
            $table->text('Implementing_Office');
            $table->string('Project_Title');
            $table->string('Grant_amount_(USD)');
            $table->string('Dates_from_GCF');
            $table->string('Start_Date');
            $table->string('Duration_(months)');
            $table->string('End_Date');
            $table->text('Readiness_or_NAP');
            $table->string('Type_of_readiness');
            $table->string('First_disbursement_amount');
            $table->text('Status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('unvprjs');
    }
}
