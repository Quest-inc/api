<!-- Project Ref Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Project_Ref', 'Project Ref:') !!}
    {!! Form::text('Project_Ref', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Country', 'Country:') !!}
    {!! Form::text('Country', null, ['class' => 'form-control']) !!}
</div>

<!-- Implementing Office Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Implementing_Office', 'Implementing Office:') !!}
    {!! Form::text('Implementing_Office', null, ['class' => 'form-control']) !!}
</div>

<!-- Project Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Project_Title', 'Project Title:') !!}
    {!! Form::text('Project_Title', null, ['class' => 'form-control']) !!}
</div>

<!-- Grant Amount (Usd) Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Grant_amount_(USD)', 'Grant Amount (Usd):') !!}
    {!! Form::text('Grant_amount_(USD)', null, ['class' => 'form-control']) !!}
</div>

<!-- Dates From Gcf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Dates_from_GCF', 'Dates From Gcf:') !!}
    {!! Form::text('Dates_from_GCF', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Start_Date', 'Start Date:') !!}
    {!! Form::text('Start_Date', null, ['class' => 'form-control']) !!}
</div>

<!-- Duration (Months) Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Duration_(months)', 'Duration (Months):') !!}
    {!! Form::text('Duration_(months)', null, ['class' => 'form-control']) !!}
</div>

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('End_Date', 'End Date:') !!}
    {!! Form::text('End_Date', null, ['class' => 'form-control']) !!}
</div>

<!-- Readiness Or Nap Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Readiness_or_NAP', 'Readiness Or Nap:') !!}
    {!! Form::text('Readiness_or_NAP', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Of Readiness Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Type_of_readiness', 'Type Of Readiness:') !!}
    {!! Form::text('Type_of_readiness', null, ['class' => 'form-control']) !!}
</div>

<!-- First Disbursement Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('First_disbursement_amount', 'First Disbursement Amount:') !!}
    {!! Form::text('First_disbursement_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Status', 'Status:') !!}
    {!! Form::text('Status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('unvprjs.index') !!}" class="btn btn-default">Cancel</a>
</div>
