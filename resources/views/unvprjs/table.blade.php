<table class="table table-responsive" id="unvprjs-table">
    <thead>
        <tr>
            <th>Project Ref</th>
        <th>Country</th>
        <th>Implementing Office</th>
        <th>Project Title</th>
        <th>Grant Amount (Usd)</th>
        <th>Dates From Gcf</th>
        <th>Start Date</th>
        <th>Duration (Months)</th>
        <th>End Date</th>
        <th>Readiness Or Nap</th>
        <th>Type Of Readiness</th>
        <th>First Disbursement Amount</th>
        <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($unvprjs as $unvprjs)
        <tr>
            <td>{!! $unvprjs->Project_Ref !!}</td>
            <td>{!! $unvprjs->Country !!}</td>
            <td>{!! $unvprjs->Implementing_Office !!}</td>
            <td>{!! $unvprjs->Project_Title !!}</td>
            <td>{!! $unvprjs->Grant_amount_USD !!}</td>
            <td>{!! $unvprjs->Dates_from_GCF !!}</td>
            <td>{!! $unvprjs->Start_Date !!}</td>
            <td>{!! $unvprjs->Duration_months !!}</td>
            <td>{!! $unvprjs->End_Date !!}</td>
            <td>{!! $unvprjs->Readiness_or_NAP !!}</td>
            <td>{!! $unvprjs->Type_of_readiness !!}</td>
            <td>{!! $unvprjs->First_disbursement_amount !!}</td>
            <td>{!! $unvprjs->Status !!}</td>
            <td>
                {!! Form::open(['route' => ['unvprjs.destroy', $unvprjs->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('unvprjs.show', [$unvprjs->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('unvprjs.edit', [$unvprjs->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
