<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $unvprjs->id !!}</p>
</div>

<!-- Project Ref Field -->
<div class="form-group">
    {!! Form::label('Project_Ref', 'Project Ref:') !!}
    <p>{!! $unvprjs->Project_Ref !!}</p>
</div>

<!-- Country Field -->
<div class="form-group">
    {!! Form::label('Country', 'Country:') !!}
    <p>{!! $unvprjs->Country !!}</p>
</div>

<!-- Implementing Office Field -->
<div class="form-group">
    {!! Form::label('Implementing_Office', 'Implementing Office:') !!}
    <p>{!! $unvprjs->Implementing_Office !!}</p>
</div>

<!-- Project Title Field -->
<div class="form-group">
    {!! Form::label('Project_Title', 'Project Title:') !!}
    <p>{!! $unvprjs->Project_Title !!}</p>
</div>

<!-- Grant Amount (Usd) Field -->
<div class="form-group">
    {!! Form::label('Grant_amount_(USD)', 'Grant Amount (Usd):') !!}
    <p>{!! $unvprjs->Grant_amount_(USD) !!}</p>
</div>

<!-- Dates From Gcf Field -->
<div class="form-group">
    {!! Form::label('Dates_from_GCF', 'Dates From Gcf:') !!}
    <p>{!! $unvprjs->Dates_from_GCF !!}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('Start_Date', 'Start Date:') !!}
    <p>{!! $unvprjs->Start_Date !!}</p>
</div>

<!-- Duration (Months) Field -->
<div class="form-group">
    {!! Form::label('Duration_(months)', 'Duration (Months):') !!}
    <p>{!! $unvprjs->Duration_(months) !!}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('End_Date', 'End Date:') !!}
    <p>{!! $unvprjs->End_Date !!}</p>
</div>

<!-- Readiness Or Nap Field -->
<div class="form-group">
    {!! Form::label('Readiness_or_NAP', 'Readiness Or Nap:') !!}
    <p>{!! $unvprjs->Readiness_or_NAP !!}</p>
</div>

<!-- Type Of Readiness Field -->
<div class="form-group">
    {!! Form::label('Type_of_readiness', 'Type Of Readiness:') !!}
    <p>{!! $unvprjs->Type_of_readiness !!}</p>
</div>

<!-- First Disbursement Amount Field -->
<div class="form-group">
    {!! Form::label('First_disbursement_amount', 'First Disbursement Amount:') !!}
    <p>{!! $unvprjs->First_disbursement_amount !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('Status', 'Status:') !!}
    <p>{!! $unvprjs->Status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $unvprjs->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $unvprjs->updated_at !!}</p>
</div>

