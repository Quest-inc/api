@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Unvprjs
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($unvprjs, ['route' => ['unvprjs.update', $unvprjs->id], 'method' => 'patch']) !!}

                        @include('unvprjs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection