<?php

use Faker\Factory as Faker;
use App\Models\Unvprjs;
use App\Repositories\UnvprjsRepository;

trait MakeUnvprjsTrait
{
    /**
     * Create fake instance of Unvprjs and save it in database
     *
     * @param array $unvprjsFields
     * @return Unvprjs
     */
    public function makeUnvprjs($unvprjsFields = [])
    {
        /** @var UnvprjsRepository $unvprjsRepo */
        $unvprjsRepo = App::make(UnvprjsRepository::class);
        $theme = $this->fakeUnvprjsData($unvprjsFields);
        return $unvprjsRepo->create($theme);
    }

    /**
     * Get fake instance of Unvprjs
     *
     * @param array $unvprjsFields
     * @return Unvprjs
     */
    public function fakeUnvprjs($unvprjsFields = [])
    {
        return new Unvprjs($this->fakeUnvprjsData($unvprjsFields));
    }

    /**
     * Get fake data of Unvprjs
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUnvprjsData($unvprjsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'Project_Ref' => $fake->word,
            'Country' => $fake->word,
            'Implementing_Office' => $fake->text,
            'Project_Title' => $fake->word,
            'Grant_amount_(USD)' => $fake->word,
            'Dates_from_GCF' => $fake->word,
            'Start_Date' => $fake->word,
            'Duration_(months)' => $fake->word,
            'End_Date' => $fake->word,
            'Readiness_or_NAP' => $fake->text,
            'Type_of_readiness' => $fake->word,
            'First_disbursement_amount' => $fake->word,
            'Status' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $unvprjsFields);
    }
}
