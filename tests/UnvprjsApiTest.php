<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UnvprjsApiTest extends TestCase
{
    use MakeUnvprjsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateUnvprjs()
    {
        $unvprjs = $this->fakeUnvprjsData();
        $this->json('POST', '/api/v1/unvprjs', $unvprjs);

        $this->assertApiResponse($unvprjs);
    }

    /**
     * @test
     */
    public function testReadUnvprjs()
    {
        $unvprjs = $this->makeUnvprjs();
        $this->json('GET', '/api/v1/unvprjs/'.$unvprjs->id);

        $this->assertApiResponse($unvprjs->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUnvprjs()
    {
        $unvprjs = $this->makeUnvprjs();
        $editedUnvprjs = $this->fakeUnvprjsData();

        $this->json('PUT', '/api/v1/unvprjs/'.$unvprjs->id, $editedUnvprjs);

        $this->assertApiResponse($editedUnvprjs);
    }

    /**
     * @test
     */
    public function testDeleteUnvprjs()
    {
        $unvprjs = $this->makeUnvprjs();
        $this->json('DELETE', '/api/v1/unvprjs/'.$unvprjs->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/unvprjs/'.$unvprjs->id);

        $this->assertResponseStatus(404);
    }
}
