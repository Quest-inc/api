<?php

use App\Models\Unvprjs;
use App\Repositories\UnvprjsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UnvprjsRepositoryTest extends TestCase
{
    use MakeUnvprjsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var UnvprjsRepository
     */
    protected $unvprjsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->unvprjsRepo = App::make(UnvprjsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateUnvprjs()
    {
        $unvprjs = $this->fakeUnvprjsData();
        $createdUnvprjs = $this->unvprjsRepo->create($unvprjs);
        $createdUnvprjs = $createdUnvprjs->toArray();
        $this->assertArrayHasKey('id', $createdUnvprjs);
        $this->assertNotNull($createdUnvprjs['id'], 'Created Unvprjs must have id specified');
        $this->assertNotNull(Unvprjs::find($createdUnvprjs['id']), 'Unvprjs with given id must be in DB');
        $this->assertModelData($unvprjs, $createdUnvprjs);
    }

    /**
     * @test read
     */
    public function testReadUnvprjs()
    {
        $unvprjs = $this->makeUnvprjs();
        $dbUnvprjs = $this->unvprjsRepo->find($unvprjs->id);
        $dbUnvprjs = $dbUnvprjs->toArray();
        $this->assertModelData($unvprjs->toArray(), $dbUnvprjs);
    }

    /**
     * @test update
     */
    public function testUpdateUnvprjs()
    {
        $unvprjs = $this->makeUnvprjs();
        $fakeUnvprjs = $this->fakeUnvprjsData();
        $updatedUnvprjs = $this->unvprjsRepo->update($fakeUnvprjs, $unvprjs->id);
        $this->assertModelData($fakeUnvprjs, $updatedUnvprjs->toArray());
        $dbUnvprjs = $this->unvprjsRepo->find($unvprjs->id);
        $this->assertModelData($fakeUnvprjs, $dbUnvprjs->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteUnvprjs()
    {
        $unvprjs = $this->makeUnvprjs();
        $resp = $this->unvprjsRepo->delete($unvprjs->id);
        $this->assertTrue($resp);
        $this->assertNull(Unvprjs::find($unvprjs->id), 'Unvprjs should not exist in DB');
    }
}
