<?php

namespace App\Repositories;

use App\Models\Unvprjs;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UnvprjsRepository
 * @package App\Repositories
 * @version February 24, 2019, 8:34 pm UTC
 *
 * @method Unvprjs findWithoutFail($id, $columns = ['*'])
 * @method Unvprjs find($id, $columns = ['*'])
 * @method Unvprjs first($columns = ['*'])
*/
class UnvprjsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Project_Ref',
        'Country',
        'Implementing_Office',
        'Project_Title',
        'Grant_amount_(USD)',
        'Dates_from_GCF',
        'Start_Date',
        'Duration_(months)',
        'End_Date',
        'Readiness_or_NAP',
        'Type_of_readiness',
        'First_disbursement_amount',
        'Status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Unvprjs::class;
    }
}
