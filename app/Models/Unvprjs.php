<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Unvprjs
 * @package App\Models
 * @version February 24, 2019, 8:34 pm UTC
 *
 * @property string Project_Ref
 * @property string Country
 * @property string Implementing_Office
 * @property string Project_Title
 * @property string Grant_amount_(USD)
 * @property string Dates_from_GCF
 * @property string Start_Date
 * @property string Duration_(months)
 * @property string End_Date
 * @property string Readiness_or_NAP
 * @property string Type_of_readiness
 * @property string First_disbursement_amount
 * @property string Status
 */
class Unvprjs extends Model
{
    use SoftDeletes;

    public $table = 'unvprjs';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'Project_Ref',
        'Country',
        'Implementing_Office',
        'Project_Title',
        'Grant_amount_(USD)',
        'Dates_from_GCF',
        'Start_Date',
        'Duration_(months)',
        'End_Date',
        'Readiness_or_NAP',
        'Type_of_readiness',
        'First_disbursement_amount',
        'Status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'Project_Ref' => 'string',
        'Country' => 'string',
        'Implementing_Office' => 'string',
        'Project_Title' => 'string',
        'Grant_amount_(USD)' => 'string',
        'Dates_from_GCF' => 'string',
        'Start_Date' => 'string',
        'Duration_(months)' => 'string',
        'End_Date' => 'string',
        'Readiness_or_NAP' => 'string',
        'Type_of_readiness' => 'string',
        'First_disbursement_amount' => 'string',
        'Status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'Project_Ref' => 'required',
        'Country' => 'required',
        'Implementing_Office' => 'required',
        'Project_Title' => 'required',
        'Grant_amount_(USD)' => 'required',
        'Dates_from_GCF' => 'required',
        'Start_Date' => 'required',
        'Duration_(months)' => 'required',
        'End_Date' => 'required',
        'Readiness_or_NAP' => 'required',
        'Type_of_readiness' => 'required',
        'First_disbursement_amount' => 'required',
        'Status' => 'required'
    ];

    
}
