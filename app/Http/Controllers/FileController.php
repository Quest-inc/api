<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UnvprjExport;
use App\Imports\UnvprjImport;
use Maatwebsite\Excel\Facades\Excel;

class FileController extends Controller

{

    /**

    * @return \Illuminate\Support\Collection

    */

    public function importExportView()

    {
       return view('import');
    }



    /**

    * @return \Illuminate\Support\Collection

    */

    public function export()

    {
        return Excel::download(new UnvprjExport, 'Unvprj.xlsx');
    }



    /**

    * @return \Illuminate\Support\Collection

    */

    public function import()

    {
        Excel::import(new UnvprjImport,request()->file('file'));
        return back();
    }
}
