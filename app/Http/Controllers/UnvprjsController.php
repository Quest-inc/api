<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUnvprjsRequest;
use App\Http\Requests\UpdateUnvprjsRequest;
use App\Repositories\UnvprjsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class UnvprjsController extends AppBaseController
{
    /** @var  UnvprjsRepository */
    private $unvprjsRepository;

    public function __construct(UnvprjsRepository $unvprjsRepo)
    {
        $this->unvprjsRepository = $unvprjsRepo;
    }

    /**
     * Display a listing of the Unvprjs.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->unvprjsRepository->pushCriteria(new RequestCriteria($request));
        $unvprjs = $this->unvprjsRepository->all();

        return view('unvprjs.index')
            ->with('unvprjs', $unvprjs);
    }

    /**
     * Show the form for creating a new Unvprjs.
     *
     * @return Response
     */
    public function create()
    {
        return view('unvprjs.create');
    }

    /**
     * Store a newly created Unvprjs in storage.
     *
     * @param CreateUnvprjsRequest $request
     *
     * @return Response
     */
    public function store(CreateUnvprjsRequest $request)
    {
        $input = $request->all();

        $unvprjs = $this->unvprjsRepository->create($input);

        Flash::success('Unvprjs saved successfully.');

        return redirect(route('unvprjs.index'));
    }

    /**
     * Display the specified Unvprjs.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $unvprjs = $this->unvprjsRepository->findWithoutFail($id);

        if (empty($unvprjs)) {
            Flash::error('Unvprjs not found');

            return redirect(route('unvprjs.index'));
        }

        return view('unvprjs.show')->with('unvprjs', $unvprjs);
    }

    /**
     * Show the form for editing the specified Unvprjs.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $unvprjs = $this->unvprjsRepository->findWithoutFail($id);

        if (empty($unvprjs)) {
            Flash::error('Unvprjs not found');

            return redirect(route('unvprjs.index'));
        }

        return view('unvprjs.edit')->with('unvprjs', $unvprjs);
    }

    /**
     * Update the specified Unvprjs in storage.
     *
     * @param  int              $id
     * @param UpdateUnvprjsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUnvprjsRequest $request)
    {
        $unvprjs = $this->unvprjsRepository->findWithoutFail($id);

        if (empty($unvprjs)) {
            Flash::error('Unvprjs not found');

            return redirect(route('unvprjs.index'));
        }

        $unvprjs = $this->unvprjsRepository->update($request->all(), $id);

        Flash::success('Unvprjs updated successfully.');

        return redirect(route('unvprjs.index'));
    }

    /**
     * Remove the specified Unvprjs from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $unvprjs = $this->unvprjsRepository->findWithoutFail($id);

        if (empty($unvprjs)) {
            Flash::error('Unvprjs not found');

            return redirect(route('unvprjs.index'));
        }

        $this->unvprjsRepository->delete($id);

        Flash::success('Unvprjs deleted successfully.');

        return redirect(route('unvprjs.index'));
    }
}
