<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUnvprjsAPIRequest;
use App\Http\Requests\API\UpdateUnvprjsAPIRequest;
use App\Models\Unvprjs;
use App\Repositories\UnvprjsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UnvprjsController
 * @package App\Http\Controllers\API
 */

class UnvprjsAPIController extends AppBaseController
{
    /** @var  UnvprjsRepository */
    private $unvprjsRepository;

    public function __construct(UnvprjsRepository $unvprjsRepo)
    {
        $this->unvprjsRepository = $unvprjsRepo;
    }

    /**
     * Display a listing of the Unvprjs.
     * GET|HEAD /unvprjs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->unvprjsRepository->pushCriteria(new RequestCriteria($request));
        $this->unvprjsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $unvprjs = $this->unvprjsRepository->all();

        return $this->sendResponse($unvprjs->toArray(), 'Unvprjs retrieved successfully');
    }

    /**
     * Store a newly created Unvprjs in storage.
     * POST /unvprjs
     *
     * @param CreateUnvprjsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUnvprjsAPIRequest $request)
    {
        $input = $request->all();

        $unvprjs = $this->unvprjsRepository->create($input);

        return $this->sendResponse($unvprjs->toArray(), 'Unvprjs saved successfully');
    }

    /**
     * Display the specified Unvprjs.
     * GET|HEAD /unvprjs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Unvprjs $unvprjs */
        $unvprjs = $this->unvprjsRepository->findWithoutFail($id);

        if (empty($unvprjs)) {
            return $this->sendError('Unvprjs not found');
        }

        return $this->sendResponse($unvprjs->toArray(), 'Unvprjs retrieved successfully');
    }

    /**
     * Update the specified Unvprjs in storage.
     * PUT/PATCH /unvprjs/{id}
     *
     * @param  int $id
     * @param UpdateUnvprjsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUnvprjsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Unvprjs $unvprjs */
        $unvprjs = $this->unvprjsRepository->findWithoutFail($id);

        if (empty($unvprjs)) {
            return $this->sendError('Unvprjs not found');
        }

        $unvprjs = $this->unvprjsRepository->update($input, $id);

        return $this->sendResponse($unvprjs->toArray(), 'Unvprjs updated successfully');
    }

    /**
     * Remove the specified Unvprjs from storage.
     * DELETE /unvprjs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Unvprjs $unvprjs */
        $unvprjs = $this->unvprjsRepository->findWithoutFail($id);

        if (empty($unvprjs)) {
            return $this->sendError('Unvprjs not found');
        }

        $unvprjs->delete();

        return $this->sendResponse($id, 'Unvprjs deleted successfully');
    }
}
