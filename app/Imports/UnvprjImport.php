<?php

namespace App\Imports;

use App\Models\Unvprjs;
use Maatwebsite\Excel\Concerns\ToModel;

class UnvprjImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $col)
    {
        return new Unvprjs([
          'Project_Ref'    => $col[0],
          'Country'        => $col[1],
          'Implementing_Office'    => $col[2],
          'Project_Title'          => $col[3],
          'Grant_amount_(USD)'     => $col[4],
          'Dates_from_GCF'         => $col[5],
          'Start_Date'             => $col[6],
          'Duration_(months)'      => $col[7],
          'End_Date'               => $col[8],
          'Readiness_or_NAP'       => $col[9],
          'Type_of_readiness'      => $col[10],
          'First_disbursement_amount'     => $col[11],
          'Status'                        => $col[12],
        ]);
    }
}
