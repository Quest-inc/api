<?php

namespace App\Exports;

use App\Unvprj;
use Maatwebsite\Excel\Concerns\FromCollection;

class UnvprjExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Unvprj::all();
    }
}
